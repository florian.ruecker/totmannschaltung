﻿#ifndef TIMER_H_
#define TIMER_H_

void TimerInitCTCMode(unsigned char tresh);
unsigned int TimerGetCount(void);
void TimerReset(void);

#endif /* TIMER_H_ */
