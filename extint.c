﻿#include "extint.h"
#include "timer.h"

#include <avr/io.h>
#include <avr/interrupt.h>

void ExtintInit(void)
{
	EICRA |= (1 << ISC00) | (1 << ISC01); // rising edge
	EIMSK |= (1 << INT0);
	sei();
}

ISR (INT0_vect)
{
	TimerReset();
}
