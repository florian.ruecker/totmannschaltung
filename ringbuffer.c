﻿#include "ringbuffer.h"

#include <avr/io.h>
#include <avr/interrupt.h>

#define RINGBUFFERSIZE 11
unsigned char ringbuffer[RINGBUFFERSIZE];
unsigned char writeIndex, readIndex;

void RingbufferInit(void)
{
	writeIndex = 0;
	readIndex = 0;
}

char RingbufferSendSerialData(char *pSerialData, unsigned char len)
{
	int freeSize;
	
	cli();
	if(writeIndex >= readIndex) {
		freeSize = RINGBUFFERSIZE - (writeIndex - readIndex) - 1;
	}
	else {
		freeSize = readIndex - writeIndex - 1;
	}
	sei();
	
	if(freeSize < len) {
		return 0;
	}
	
	for(int i=0; i<len; i++) {
		ringbuffer[writeIndex] = pSerialData[i];
		writeIndex++;
		if(writeIndex >= RINGBUFFERSIZE) {
			writeIndex = 0;
		}
	}
	
	UCSR0B |= (1 << UDRIE0);
	return 0;
}

ISR (USART0_UDRE_vect)
{
	if(readIndex == writeIndex) {
		UCSR0B &= ~(1 << UDRIE0);
		return;
	}
	UDR0 = ringbuffer[readIndex];
	readIndex ++;
	if(readIndex >= RINGBUFFERSIZE) {
		readIndex = 0;
	}
}
