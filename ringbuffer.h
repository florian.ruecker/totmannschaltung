﻿#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

void RingbufferInit(void);
char RingbufferSendSerialData(char *pSerialData, unsigned char len);

#endif /* RINGBUFFER_H_ */
