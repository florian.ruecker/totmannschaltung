#define F_CPU 16000000

#include "serial.h"
#include "timer.h"
#include "ringbuffer.h"
#include "extint.h"

#include <avr/io.h>

int main(void)
{
	char text[] = "toter Mann!";
	SerialInit(103);
	TimerInitCTCMode(15624);
    while (1) 
    {
		if(TimerGetCount() == 30) {
			RingbufferSendSerialData(text, sizeof(text));
		}
    }
}
