﻿#include "timer.h"

#include <avr/io.h>
#include <avr/interrupt.h>

unsigned int var = 0;

void TimerInitCTCMode(unsigned char tresh)
{
	  // CTC Mode
	  TCCR0A &= ~(1 << WGM00);
	  TCCR0A |= (1 << WGM01);
	  TCCR0B &= ~(1 << WGM02);
	  
	  // Prescaler 1024
	  TCCR0B |= (1 << CS00) | (1 << CS02);
	  TCCR0B &= ~(1 << CS01);	  
	  // Interrupt	  OCR0A = tresh;	  TIMSK0 |= (1 << OCIE0A);	  sei();
}

ISR (TIMER0_COMPA_vect)
{
	var ++;
}

unsigned int TimerGetCount(void)
{
	return var;
}

void TimerReset(void)
{
	var = 0;
}
